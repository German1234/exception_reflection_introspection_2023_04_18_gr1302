def summa(*arg):
    if all([isinstance(el, str) for el in arg]):
        sum = ""
    elif all([isinstance(el, (int, float, bool)) for el in arg]):
        sum = 0
    else:
        raise ValueError("arg error")
    for elem in arg:
        sum += elem
    return sum


if __name__ == "__main__":
    assert (summa(1, 2, 3) == 6), "Error"
    assert (summa("a", "b", "c") == "abc"), "str error"
    
    print(__name__)
    print("все пацює нормально")
